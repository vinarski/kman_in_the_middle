#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>

#define PORT 3334
#define BUFFER_SIZE 1024

int main ()
{
    int sockfd;
    int len;
    struct sockaddr_in address;
    int result;
    char buffer[BUFFER_SIZE];

    if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("\n Socket creation error \n");
        exit (EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    //todo: ip from a config file
    address.sin_addr.s_addr = inet_addr ("127.0.0.1");
    address.sin_port = htons(PORT);
    len = sizeof (address);

    result = connect (sockfd, (struct sockaddr *) &address, len);
    if (result == -1)
    {
        perror ("oops: client socket setup failed");
        exit (EXIT_FAILURE);
    }

    printf ("Input some text. Enter 'end' to finish...\n");
    while (strncmp ("end", buffer, 3) != 0)
    {
        int len;
        fgets (buffer, BUFFER_SIZE, stdin);
        len = strlen (buffer);
        if (buffer[len - 1] == '\n')
            buffer[len - 1] = 0;
        write (sockfd, buffer, len);
        read (sockfd, &buffer, BUFFER_SIZE);
        printf ("client: server said: %s\n", buffer);
    }

    close (sockfd);
    exit (EXIT_SUCCESS);
}
