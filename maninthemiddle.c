#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h> //for gethostbyname
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0
#define MAX_CLIENTS 15
#define BUFFER_SIZE 1024
#define MAX_BACK 5

typedef struct sockets
{
    int client_fd;
    int server_fd;

} socks_t;


typedef struct config
{
    long port_local;
    long port_remote;
    char server_remote[BUFFER_SIZE + 1];

} params_t;

static long convert_str_to_long (const char *str)
{
    long value;
    char *end;

    if (str[0] == '\0')
    {
        printf("\nconfig file is corrupt\n");
        exit (EXIT_FAILURE);
    }
    //discards any whitespace characters (as identified by calling isspace()
    value = strtol (str, &end, 10);

    /* If the result is 0, test for an error */
    if (value == 0 || value == LONG_MIN || value == LONG_MAX)
    {
        /* If a conversion error occurred, display a message and exit */
        if (errno == EINVAL)
        {
            printf("config file is corrupt - conversion error occurred: %d\n",
                   errno);
            exit (EXIT_FAILURE);
        }

        /* If the value provided was out of range, display a warning message */
        if (errno == ERANGE)
        {
            printf("config file is corrupt - the value provided was out of range\n");
            exit (EXIT_FAILURE);
        }

        printf("config file is corrupt - the value is 0\n");
        exit (EXIT_FAILURE);
    }

    return value;
}


static params_t read_config_file (void)
{
    static int func_call_num;
    static params_t params;

    long line_num;
    char buffer[BUFFER_SIZE + 1];
    FILE *config_file = NULL;

    func_call_num ++;
    if (func_call_num > 1)
        return params;

    config_file = fopen ("config", "r");
    if (config_file == NULL)
    {
        printf("\nfailure opening config file\n");
        exit (EXIT_FAILURE);
    }

    for (line_num = 1; ;line_num ++)
    {
        char *ret;
        char *equals_char = "="; //config file separator
        char *equals_char_ptr;
        char *third_line_value;

        //fgets vozvrashyaet kak dopolnitelnyj servis adres stroki arrBuffer
        ret = fgets (buffer, BUFFER_SIZE, config_file);
        //DEBUG printf ("ret addr: %p, buffer addr: %p\n", ret, buffer);
        if (ret == NULL)
            break;

        //DEBUG printf ("\ncontents line #%d: %s\n", line_num, buffer);
        equals_char_ptr = strstr (buffer, equals_char);
        if (equals_char_ptr == NULL)
        {
            printf("\nconfig file is corrupt\n");
            exit (EXIT_FAILURE);
        }

        equals_char_ptr ++;
        third_line_value = equals_char_ptr;

        //DEBUG printf ("line value: %s\n", third_line_value);
        if (line_num == 1)
            params.port_local = convert_str_to_long (third_line_value);
        else if (line_num == 2)
            params.port_remote = convert_str_to_long (third_line_value);
        else if (line_num == 3)
        {
            int l = strlen (third_line_value);

            if (l != 0)
                memcpy (params.server_remote, third_line_value, l);
            params.server_remote[l] = 0;

            /* delete lf */
            /* ------------ */
            if (params.server_remote[l - 1] == '\n')
                params.server_remote[l - 1] = 0;

            break; //further lines are not of interest
        }
    }

    fclose (config_file);

    return params;
}


static int is_ascii (char c)
{
    if ((c < 32 || c > 127) && c != 10 && c!= 13)
        return FALSE;
    else
        return TRUE;
}


static void log_communication (const char *buffer, int communication_side,
                    int num_bytes)
{
    int i;

    if (communication_side)
        printf ("\n>>>>>>>>>>>>>>>>>>>>>>>> client says: (%dbytes)\n",
                num_bytes);
    else
        printf ("<<<<<<<<<<<<<<<<<<<<<<<< server replies: (%dbytes)\n",
                num_bytes);

    for (i = 0; i < num_bytes; i ++)
    {
        if (is_ascii (buffer[i]))
            printf ("%c", buffer[i]);
        else
            printf ("[%02X]", ((unsigned char *)buffer)[i]);
    }
    printf ("\n");
}


static int is_http (const char *request)
{
    const char *http_str = "HTTP/1.1";
    char *http_str_ptr;

    http_str_ptr = strstr (request, http_str);
    if (http_str_ptr == NULL)
        return FALSE;
    else
        return TRUE;
}

static int replace_str (const char *buffer, const char *str_delete,
                        const char *str_replace, char *res_buffer)
{
    const char *beginning_str_delete = strstr (buffer, str_delete);
    if (beginning_str_delete == NULL)
        return FALSE;

    int len = beginning_str_delete - buffer;
    memcpy (res_buffer, buffer, len);

    res_buffer[len] = 0;
    strcat (res_buffer, str_replace);

    len += strlen (str_delete);

    strcat (res_buffer, buffer + len);

    return TRUE;
}

static int add_host_to_client_request (const char *buffer, char *res_buffer)
{
    char res_buffer2[BUFFER_SIZE + 1];
    params_t params = read_config_file ();
    char str_port[6];
    sprintf(str_port, "%ld", params.port_local);

    /* instead of "Host: localhost:3334" => "Host: server_remote" */
    //determine the line to be replaced
    char line_host[50];
    line_host[0] = 0;
    strcat (line_host, "localhost:");
    strcat (line_host, str_port);

    if (!replace_str (buffer, line_host, params.server_remote, res_buffer))
        return FALSE;

/*
A recipient MUST ignore If-Modified-Since if the request contains an
If-None-Match header field; the condition in If-None-Match is
considered to be a more accurate replacement for the condition in
If-Modified-Since, and the two are only combined for the sake of
interoperating with older intermediaries that might not implement
If-None-Match.
If-None-Match: "1fd-3b94038231240"
*/

    const char *beginning_line_match = "If-None-Match:";
    char *beginning_line_match_ptr;
    beginning_line_match_ptr = strstr (res_buffer, beginning_line_match);
    if (beginning_line_match_ptr != NULL)
        beginning_line_match_ptr[17] ++;

//If-Modified-Since: Wed, 19 Mar 2003 21:46:41 GMT
    const char *beginning_line_mod = "If-Modified-Since:";
    char *beginning_line_mod_ptr;
    beginning_line_mod_ptr = strstr (res_buffer, beginning_line_mod);
    if (beginning_line_mod_ptr != NULL)
        beginning_line_mod_ptr[19] ++;

    //Refer
    char str_localhost[30];
    str_localhost[0] = 0;
    strcat (str_localhost, "localhost:");
    strcat (str_localhost, str_port);
    if (replace_str (res_buffer, str_localhost, params.server_remote,
        res_buffer2))
        memcpy (res_buffer, res_buffer2, BUFFER_SIZE);

    //Cache-Control: max-age=
    //Cache-Control: no-store
    const char *line_age = "max-age=0";
    const char *line_nostore = "no-store";
    if (replace_str (res_buffer, line_age, line_nostore, res_buffer2))
        memcpy (res_buffer, res_buffer2, BUFFER_SIZE);

    return TRUE;
}


static int create_named_proxy_socket (void)
{
    struct sockaddr_in proxy_address;
    int proxy_sock_fd;
    int server_len;
    int on = 1;

    params_t params = read_config_file ();

    if ((proxy_sock_fd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("\n proxy socket creation error \n");
        exit (EXIT_FAILURE);
    }

    //this setting makes the port reuseable
    //otherwise when the program stops and restarts,
    //the port is no longer available
    if (setsockopt (proxy_sock_fd, SOL_SOCKET, SO_REUSEADDR,
                    &on, sizeof (on)) == - 1) {}

    proxy_address.sin_family = AF_INET;
    proxy_address.sin_addr.s_addr = htonl (INADDR_ANY);
    proxy_address.sin_port = htons (params.port_local);
    server_len = sizeof (proxy_address);

    on = bind (proxy_sock_fd, (struct sockaddr *) &proxy_address, server_len);
    if (on != 0)
    {
        printf("\n proxy socket creation error occurred (on call of bind): %d\n",
               errno);
        exit (EXIT_FAILURE);
    }
    return proxy_sock_fd;
}


static int create_named_server_socket (void)
{
    struct sockaddr_in server_address;
    params_t params = read_config_file ();
    int server_sock_fd;
    int result;
    int len;
    struct hostent *hst;

    //works either way (name to ip addr, or ip addr to 4 bytes ip addr)
    printf ("server remote: <%s>\n", params.server_remote);

    hst = gethostbyname (params.server_remote);
    if (hst == NULL)
    {
        herror ("gethostbyname");
        exit (EXIT_FAILURE);
    }

    if ((server_sock_fd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror ("proxy to remote server socket creation error");
        exit (EXIT_FAILURE);
    }

    memset(&server_address, 0, sizeof (server_address));
    server_address.sin_family = AF_INET;
    memcpy (&server_address.sin_addr, hst->h_addr, hst->h_length);
    server_address.sin_port = htons(params.port_remote);
    len = sizeof (server_address);

    result = connect (server_sock_fd, (struct sockaddr *) &server_address, len);
    if (result == -1)
    {
        perror ("oops: proxy to remote server socket setup failed");
        exit (EXIT_FAILURE);
    }

    return server_sock_fd;
}


static void *client_to_server_communication (socks_t *arg)
{
    int client_sock_fd, server_socket_fd;
    char buffer[BUFFER_SIZE + 1];
    client_sock_fd = arg->client_fd;
    server_socket_fd = arg->server_fd;

    while (TRUE)
    {
        int res = read (client_sock_fd, buffer, BUFFER_SIZE);
        if (res <= 0)
        {
            close (client_sock_fd);
            close (server_socket_fd);

            printf ("\nclient->server: removing the client on fd: %d"
                    ", remote server fd: %d\n", client_sock_fd, server_socket_fd);
            return NULL;
        }

        buffer[res] = 0;

        if (is_http (buffer))
        {
            char buffer_add_host[BUFFER_SIZE + 1];
            if (add_host_to_client_request (buffer, buffer_add_host))
            {
                strcpy (buffer, buffer_add_host);
                res = strlen (buffer);
            }
        }

        log_communication (buffer, TRUE, res);

        res = write (server_socket_fd, buffer, res);
        if (res <= 0)
        {
            close (client_sock_fd);
            close (server_socket_fd);

            printf ("\nclient->server: removing the client on fd: %d"
                    ", remote server fd: %d\n", client_sock_fd, server_socket_fd);
            return NULL;
        }
    }
}


static void *server_to_client_communication (const socks_t *arg)
{
    //listen to the web server
    int client_sock_fd, server_socket_fd;
    char buffer[BUFFER_SIZE + 1];

    client_sock_fd   = arg->client_fd;
    server_socket_fd = arg->server_fd;

    while (TRUE)
    {
        int res;
        res = read (server_socket_fd, &buffer, BUFFER_SIZE);
        if (res <= 0)
        {
            close (client_sock_fd);
            close (server_socket_fd);
            printf ("\nserver->client: removing the client on fd: %d"
                    ", remote server fd: %d\n", client_sock_fd, server_socket_fd);
            break;
        }

        buffer[res] = 0;
        log_communication (buffer, FALSE, res);

        res = write (client_sock_fd, buffer, res);
        if (res <= 0)
        {
            close (client_sock_fd);
            close (server_socket_fd);

            printf ("\nserver->client: removing the client on fd: %d"
                    ", remote server fd: %d\n", client_sock_fd, server_socket_fd);
            return NULL;
        }
    }
}

static void connect_new_clients (int *proxy_sockfd)
{
    while (TRUE)
    {
        socks_t sox;
        pthread_t client_to_server_communication_thread;
        pthread_t server_to_client_communication_thread;

        int res;
        int client_sockfd;
        int server_socketfd;

        int client_len;
        struct sockaddr_in client_address;

        printf("server waiting for connections\n");

        //new connection to additional client
        client_len = sizeof (client_address);
        client_sockfd = accept (*proxy_sockfd,
                       (struct sockaddr *) &client_address, &client_len);
        printf ("adding client on fd %d\n", client_sockfd);

       //create and name a socket for the remote server
        server_socketfd = create_named_server_socket ();
        printf ("client successfully connected to the remote server on fd %d\n",
                server_socketfd);

        sox.client_fd = client_sockfd;
        sox.server_fd = server_socketfd;

        //start thread client-to-server side
        res = pthread_create (&client_to_server_communication_thread, NULL,
                              (void*)&client_to_server_communication, &sox);
        if (res != 0)
        {
            perror ("client_to_server_communication_thread creation failed!");
            exit (EXIT_FAILURE);
        }

        //start thread server-to-client side
        res = pthread_create (&server_to_client_communication_thread, NULL,
                              (void*)&server_to_client_communication, &sox);
        if (res != 0)
        {
            perror ("server_to_client_communication_thread creation failed!");
            exit (EXIT_FAILURE);
        }
    }
}


int main (void)
{
    int proxy_socket_fd;

    //create and name a socket for the proxy server
    proxy_socket_fd = create_named_proxy_socket ();

    //create a connection queue
    int res = listen (proxy_socket_fd, MAX_BACK);
    if (res != 0)
    {
        printf("\n proxy socket creation error occurred (on call of listen): %d\n",
               errno);
        exit (EXIT_FAILURE);
    }

    //wait for clients
    connect_new_clients (&proxy_socket_fd);

    return 0;
}

