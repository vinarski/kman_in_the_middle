#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>

#define PORT 3334
#define MAX_BACKLOG 5
#define BUFFER_SIZE 1024


int main ()
{
    int server_sockfd, client_sockfd;
    int server_len, client_len;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    int result;
    fd_set readfds, testfds;
    struct timeval timeout;

    //create and name a socket for the server
    server_sockfd =socket (AF_INET, SOCK_STREAM, 0);

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl (INADDR_ANY);
    server_address.sin_port = htons (PORT);
    server_len = sizeof (server_address);

    bind (server_sockfd, (struct sockaddr *) &server_address, server_len);

    //create a connection queue and init readfds to handle input from sercer_sockfd
    listen (server_sockfd, MAX_BACKLOG);

    FD_ZERO (&readfds);
    FD_SET (server_sockfd, &readfds);

    //wait for clients and requests
    //null pointer as the timeout means no timeout will occur
    //the program will exit and report an error if select returns a value less than 1 (errno will be set to describe the error)
    while (1)
    {
        //timeout.tv_sec = 10; //10 seconds
        //timeout.tv_usec = 500000; //0.5 seconds
        char buffer[BUFFER_SIZE];
        int fd;
        int nread;
    
        testfds = readfds;

        printf ("server waiting\n");
        result = select (FD_SETSIZE, &testfds, (fd_set *)0, (fd_set *)0, (struct timeval *) 0);

        if (result < 1)
        {
            perror ("server");
            exit (1);
        }

        //once there is acivity, find which descriptor it's on by checking each in turn using FD_ISSET (macro which checks if the fd is element of the set)
        for (fd = 0; fd < FD_SETSIZE; ++fd)
            if (FD_ISSET (fd, &testfds))
            {
                //if the activity is on server_sockfd, it must be a request dor a new connection - add the associated client_sockfd to the descriptor set
                if (fd == server_sockfd)
                {
                    client_len = sizeof (client_address);
                    client_sockfd = accept (server_sockfd, (struct sockaddr *) &client_address, &client_len);
                    FD_SET (client_sockfd, &readfds);
                    printf ("adding client on fd %d\n", client_sockfd);
                }
                //if it isn't the server, it must be client activity. If close is received, the client has gone away - remove it from the descriptor set.
                //otherwise serve the client
                else
                {
                    ioctl (fd, FIONREAD, &nread);

                    if (nread == 0)
                    {
                        close (fd);
                        FD_CLR (fd, &readfds);
                        printf ("removing the client on fd %d\n", fd);
                    }
                    else
                    {
                        read (fd, &buffer, BUFFER_SIZE);
                        //sleep (2);
                        printf ("serving client on fd %d\n", fd);
                        write (fd, buffer, BUFFER_SIZE);
                    }
                }
            } 
    }    
}



